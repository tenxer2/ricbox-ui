App.config(function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix("");
    $routeProvider
        .when('/', {
            // controller: 'HomeController',
            templateUrl: 'app/page/home/home.html'
        })
        .when('/ricbox', {
            // controller: 'HomeController',
            templateUrl: 'app/page/ricbox/ricbox.html'
        })
        .otherwise({
            redirectTo: '/error'
        });
});