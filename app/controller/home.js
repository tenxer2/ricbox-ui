App.controller('RicBoxController', function ($rootScope, $scope, $http) {
    $scope.test = 'ABHI';
    $scope.ricboxData = null;
    $scope.socketCreated = false;
    $scope.showData = null;
    $scope.data = {}
    var socket = null


    $scope.configSocket = (family, product) => {
        // Connection opened
        socket = new WebSocket('ws://localhost:5050');
        socket.addEventListener('open', function (event) {
            socket.send(JSON.stringify({
                'cmd': 'create',
                'family': family,
                'product': product
            }));
        });

        // Listen for messages
        socket.addEventListener('message', function (event) {
            backend_data = JSON.parse(event.data)
            switch(backend_data["cmd"]){
                case "created":
                    $scope.ricboxData = backend_data["ricbox"];
                    console.log("CREATED");
                    test = backend_data["ricbox"]
                    for(var tab in test){
                        if(JSON.stringify(test[tab]["sub_field"]) == '{}'){
                            for(var field_name in test[tab]['field']){
                                
                                $scope.data[field_name] = test[tab]['field'][field_name];
                                // console.log("1> :",field_name," : ",$scope.data[field_name]["value"]);
                                $scope.data[field_name]["model"] = $scope.data[field_name]["value"];
                                // console.log("2> :",field_name," : ",$scope.data[field_name]["model"]);

                                if($scope.data[field_name]['type'] === "checkbox"){
                                    if($scope.data[field_name]['model'] == "False" || $scope.data[field_name]['model'] == "0"){
                                        $scope.data[field_name]["model"] = false;
                                    }else{
                                        $scope.data[field_name]["model"] = true;
                                    };
                                };
                            };
                        }else{
                            for(var sub_tab in test[tab]['sub_field']){
                                for(var sub_field_name in test[tab]['sub_field'][sub_tab]){
                                    $scope.data[sub_field_name] = test[tab]['sub_field'][sub_tab][sub_field_name];
                                    $scope.data[sub_field_name]["model"] = $scope.data[sub_field_name]["value"];

                                    if($scope.data[sub_field_name]['type'] === "checkbox"){
                                        if($scope.data[sub_field_name]['model'] == "False" || $scope.data[sub_field_name]['model'] == '0'){
                                            $scope.data[sub_field_name]["model"] = false;
                                        }else{
                                            $scope.data[sub_field_name]["model"] = true;
                                        };
                                    };
                                    
                                    // $scope.data[sub_field_name]["lock"] = false;
                                    // console.log(sub_field_name," : ",$scope.data[sub_field_name]);
                                    // console.log("json :",test[tab]['sub_field'][sub_tab][sub_field_name]);
                                };
                            };
                        };
                    };
                    // console.log("NEW DATA:",$scope.data);
                break;

                case "locked":
                    key = backend_data["field"]
                    $scope.data[key]['error'] = backend_data["field_error"];
                    $scope.global_error = backend_data["global_error"]["error"]
                    $scope.global_warning = backend_data["global_error"]["warning"]
                    console.log("Error :",$scope.global_error,"\nWarning :",$scope.global_warning)
                    // console.log("locl :",backend_data["modified_field"])
                    for(field in backend_data["modified_field"]){
                        // console.log("Field :",field," Value :",backend_data["modified_field"][field]);
                        // $scope.data[field]['model'] = backend_data["modified_field"][field]
                        if($scope.data[field] != undefined){
                            
                            if($scope.data[field]['type'] === "checkbox"){
                                if($scope.data[field]['model'] == "False" || $scope.data[field]['model'] == '0'){
                                    $scope.data[field]["model"] = false;
                                }else{
                                    $scope.data[field]["model"] = true;
                                };
                            }else{
                                $scope.data[field]['model'] = backend_data["modified_field"][field]
                            };

                        };
                        
                    };
                break;
            };
            // if(backend_data["cmd"] == "created"){
            //     $scope.ricboxData = backend_data["ricbox"]
            // };
            console.log('Message from server ', backend_data);
            $scope.$apply(function () {
                $scope.socketCreated = true;
            });


        });

        console.log('SOCKET iS CREATED', $scope.socketCreated, family, product)
    };

    $scope.send_msg = () => {
        socket.send('HELLO')
    };

    $scope.tabContent = (tab) => {
        var sub = $scope.ricboxData[tab]['sub_field']
        if (JSON.stringify(sub) != '{}') {
            $scope.subField = true;
            $scope.subFieldData = sub
            $scope.showData = null
        } else {
            $scope.subField = false;
            $scope.showData = $scope.ricboxData[tab]['field']
        };

    };

    $scope.subTabContent = (tab) => {
        $scope.showData = $scope.subFieldData[tab]
    };


    $scope.set_and_lock = (key) => {
        console.log('KEY :', key);
        if ($scope.data[key]['lock'] == undefined) {
            $scope.data[key]['lock'] = false;
        };
        if ($scope.data[key]['lock'] == true) {
            socket.send(JSON.stringify({
                "cmd": "unlock",
                "field": key
            }));
            $scope.data[key]['lock'] = false;
        } else {
            socket.send(JSON.stringify({
                "cmd": "lock",
                "field": key,
                "value": $scope.data[key]['model']
            }));
            $scope.data[key]['lock'] = true;
        };
        console.log($scope.data[key]['lock'], ' : ', $scope.data[key]['model']);
        
    };


    $scope.return_lock = (key) => {
        if ($scope.data[key]['lock'] == undefined) {
            return false
        };
        if (!$scope.data[key]['lock'])
            return false;
        else
            return true;
    };
});