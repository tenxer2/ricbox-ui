App.directive('topbar', function () {

    // A configuration object defining how our directive will work
    return {
        restrict: 'E', // this defines as the type of directive (E for Element)
        // template:'<div class="sidebar"><a href="#/home">Home</a><a href="#/board_logs">Board Logs</a><a href="#/rpi">RPi</a></div>'
        templateUrl: "app/page/topbar/topbar.html" // URL of the HTML page
        // template: sidebar
    };
});

App.directive('sidebar', function () {

    // A configuration object defining how our directive will work
    return {
        restrict: 'E', // this defines as the type of directive (E for Element)
        // template:'<div class="sidebar"><a href="#/home">Home</a><a href="#/board_logs">Board Logs</a><a href="#/rpi">RPi</a></div>'
        templateUrl: "app/page/sidebar/sidebar.html" // URL of the HTML page
        // template: sidebar
    };
});